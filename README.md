# Prototyp CO2 Dashboard

Seit dem Sommer 2021 wurden durch die Stadtwerke-Tochter, die TraveKom GmbH, im Auftrag der Hansestadt Lübeck die flächendeckende Installation von Raumluftsensoren mit modernster Funktechnologie in den rund 2.200 Klassenräumen der Lübecker Schulen sowie in den Räumen der 28 städtischen Kindertagesstätten installiert. Das Ampelsystem gilt auch für die Klassenräume, in denen zur Unterstützung der Luftreinhaltung mobile Luftfilter aufgestellt wurden.

Die Raumluftqualität wird mit diesen CO2-Ampeln laufend überwacht und die Messwerte zentral in einer Datenbank datenschutzkonform verarbeitet. Mit dem optischen Lichtsignal (rot, gelb, grün) kann im Klassenraum vor Ort festgestellt werden, wann gelüftet werden muss. Die Hansestadt Lübeck kann die gewonnenen Werte nutzen, um zum Beispiel hohe Luftfeuchtigkeit frühzeitig zu erkennen, in Folge Schimmelbildung vorzubeugen oder Klassenräume zu ermitteln, in denen zusätzliche Maßnahmen zum Lüften zum Beispiel durch technische Installationen nötig sind. Im Falle einer Pandemie kann das Gesundheitsamt die Raumluftwerte zum Gesundheitsschutz für jede einzelne Klasse auswerten.

In der Vergangenheit wurde auf Basis der Software „Masterportal“ ein CO2-Cockpit erstellt, in welchem die CO2-Werte ausgewertet werden können. (https://co2-cockpit.smart-hl.city)

In den hier vorliegenden Prototypen wurde dieselbe Mechanik wie in dem CO2-Cockpit benutzt, um die Messwerte auf einem interaktiven Dashboard darzustellen. Aufgrund des hohen Datenaufkommens wurde sich hier jedoch darauf beschränkt, nur sechs Schulen und Kitas in die Darstellung mit aufzunehmen. Bei einem produktiven System muss auf eine skalierbare und robuste Vorprozessierung der Daten geachtet werden.

Im Dashboard können die Einrichtungen gefiltert und über die Karte ausgewählt werden. Ebenfalls ist eine Auswahl auch über das DropDown Feld möglich. Bei weniger als fünf Räumen, wird aus Datenschutzgründen keine Auswertung angezeigt.

<img src="https://gitlab.com/travekom-open-source/smart-city-hansestadt-l-beck/prototyp-co2-dashboard/-/raw/main/Prototyp_CO2Dashboard.png">

## Installation
Zur Installation der App kann Budibase in der Cloud- oder onPremise Variante genutzt werden. Beim Erstellen einer neuen App kann dann das hier hinterlegte Archiv importiert werden.

## Nutzung
Die App ist nicht für eine produktive Nutzung gedacht, sondern wurde im Rahmen einen Domain Prototypings erstellt um als Grundlage für weitere Projekte zu dienen. Das per iFrame eingebundene Widget von wheelmap.org muss bei produktiver Nutzung gegen eine jährliche Gebühr beim Anbieter freigeschaltet werden.

## Support
Anfragen zum Projekt an team_trk_udp@travekom.de

## Contributing
Veränderungen am Sourcecode können in Rahmen von Feature-Branches mit anschließendem MergeRequest vorgenommen werden. Der MergeReqeust wird von einem Maintainer der reviewed.

## License
Das Projekt steht unter der Apache 2.0 Lizenz
